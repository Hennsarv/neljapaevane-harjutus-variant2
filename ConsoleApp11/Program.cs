﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ConsoleApp11
{
    class Program
    {
        static void Main(string[] args)
        {
            Dictionary<string, int> vanused = new Dictionary<string, int>();
            string nimi;

            do
            {
                Console.Write("Anna nimi: ");
                nimi = Console.ReadLine();
                if (nimi != "") vanused.Add(nimi, 0);
            } while (nimi != "");

            foreach (var x in vanused.Keys.ToList())
            {
                Console.Write($"Anna {x} vanus: ");
                int vanus = int.Parse(Console.ReadLine());
                vanused[x] = vanus;
            }

            foreach(var x in vanused)
                Console.WriteLine($"{x.Key} vanus on {x.Value}");

            Console.WriteLine($"Keskmine vanus on {vanused.Values.Average()}");

        }
    }
}
